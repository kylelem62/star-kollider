package gamecomponents;

import android.opengl.GLES20;

import com.kylelem.starkollider.riGraphicTools;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

/**
 * Created by kyle on 12/19/15.
 */
public class Particle {

    /**
     * Indicates the order in which the buffer points should be drawn when rendered.
     */
    private short indicies[]={0,1,2};
    /**
     * Point buffer. contains the points in groups of three that will be drawn to create triangles
     * by OpenGL.
     */
    private float buffer[]=new float[9];
    private Color c;
    /**
     * Buffer for the point vertices.
     */
    private FloatBuffer vertexBuffer;
    /**
     * Buffer for the indices.
     */
    private ShortBuffer drawListBuffer;
    /**
     * Enabled Boolean
     */
    private boolean enabled;
    /**
     * Radial coordinates for this ParticleChaser object.
     */
    private float angle, size, startingDistance, distance;

    /**
     * Sets the coordinate value for this ParticleBuffer
     * @param angle Angle in radians. Sets the angle for this object.
     * @param size Size of the particle. Sets the width in float values.
     * @param distance Distance from center of screen. Sets the distance from the center point.
     */
    public void create(float angle, float size,float distance, Color c)
    {
        //Set instance fields.
        this.angle=angle;
        this.size=size;
        this.startingDistance=distance;
        this.distance=distance;
        this.c = c;
    }

    /**
     * Calculates the vertices of this object and puts them in the VertexBuffer.
     */
    public void calculate(){
        //Get radian value from angle and width.
        float rad = (float) ((angle-size/2) * 2 * Math.PI);

        //Set vertices in X, Y, Z order. Z is always 0
        buffer[0] = (float) ( distance * Math.cos(rad));
        buffer[1] = (float) (distance * Math.sin(rad));
        buffer[2]=0;
        rad = (float) ((angle) * 2*Math.PI);
        buffer[3] = (float) ((distance+.01) * Math.cos(rad));
        buffer[4] = (float) ((distance+.01) * Math.sin(rad));
        buffer[5]=0;
        rad = (float) ((angle+size/2) * 2*Math.PI);
        buffer[6] = (float) (distance * Math.cos(rad));
        buffer[7] = (float) (distance * Math.sin(rad));
        buffer[8]=0;

        //Create the vertexBuffer object from buffer[]
        ByteBuffer bb = ByteBuffer.allocateDirect(buffer.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(buffer);
        vertexBuffer.position(0);

        //Create the drawListBuffer from indicies[]
        ByteBuffer dlb = ByteBuffer.allocateDirect(indicies.length * 2);
        dlb.order(ByteOrder.nativeOrder());
        drawListBuffer = dlb.asShortBuffer();
        drawListBuffer.put(indicies);
        drawListBuffer.position(0);
    }

    public void move(float distance, float angle){
        this.angle = angle;
        this.distance += distance;
        if(this.distance < startingDistance-0.2f)
            this.distance = startingDistance;
    }

    public void enable(){
        this.enabled=true;
    }

    public void disable(){
        this.enabled=false;
    }

    /**
     * Draws the ParticleChaser. The color is preset to white.
     */
    public void draw(float[] m) {
        if (!enabled)
            return;
        //Set the color to white
        c.use();

        // get handle to vertex shader's vPosition member
        int mPositionHandle = GLES20.glGetAttribLocation(riGraphicTools.sp_SolidColor, "vPosition");

        // Enable generic vertex attribute array
        GLES20.glEnableVertexAttribArray(mPositionHandle);

        // Prepare the triangle coordinate data
        GLES20.glVertexAttribPointer(mPositionHandle, 3,
                GLES20.GL_FLOAT, false,
                0, vertexBuffer);

        // Get handle to shape's transformation matrix
        int mtrxhandle = GLES20.glGetUniformLocation(riGraphicTools.sp_SolidColor, "uMVPMatrix");

        // Apply the projection and view transformation
        GLES20.glUniformMatrix4fv(mtrxhandle, 1, false, m, 0);

        // Draw the triangle
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, drawListBuffer.capacity(),
                GLES20.GL_UNSIGNED_SHORT, drawListBuffer);
        drawListBuffer.clear();
        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPositionHandle);
    }
}
