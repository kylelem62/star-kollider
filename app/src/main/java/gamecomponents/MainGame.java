package gamecomponents;

import com.kylelem.starkollider.riGraphicTools;

import android.opengl.GLES20;
import android.opengl.GLU;
import android.util.Log;

public class MainGame implements Runnable{
    public static Circle centerCircle = new Circle(100,0.06f,0,0);
    public static Circle centerCore = new Circle(100,0.03f,0,0);
    public static float width = 1;//Height always equals 1, width changes from phone to phone
    static Color centerColor;
    static ColorDisplay colorDisplay;
    static Objective objective;
    public static Trapezoid t[] = new Trapezoid[10];
    public static boolean paused = false;
    public static boolean gameOn=true,
                            goodToRender=true;

	public MainGame() {
		
	}

    public static Objective getObjective(){
        return objective;
    }

	public static void click(float radius, float rad)
	{
		for(int i = 0; i<10; i++)
		{
			t[i].click(radius, rad);
		}
	}
	
	public static void draw(float[] m)
	{
		
		for(int i = 0; i<10; i++)
		{
			if(t[i].move(-0.003f,objective.getHecticity()))
				centerColor.addColorTo(t[i].color);

		}
		for(int i=0;i<10;i++)
			t[i].draw(m);
		centerColor.use();
		centerCircle.draw(m);

        objective.getColor().use();
        centerCore.draw(m);
        if(objective.getObj()==Objective.OBJ_ELIMINATE) {
            Color.BLACK.use();
            centerCore.drawSlash(m);
        }

        objective.calculate();
        objective.draw(m);
        colorDisplay.calculate();
        colorDisplay.draw(m);

	}

	public static void glInit()
	{
		centerColor = new Color(0,1,0,1);
		colorDisplay = new ColorDisplay(-0.5f,0.35f,1f,0.005f);
        colorDisplay.setColor(centerColor);
        colorDisplay.calculate();
		objective = new Objective(-0.5f,0.3475f,1f,0.01f);
        objective.setColor(centerColor);
        objective.calculate();
		for(int i = 0; i<t.length; i++)
		{
			t[i]=new Trapezoid();
		}
		Thread t = new Thread(new MainGame());
		t.start();
	}

    public static void onPause(){
        gameOn = false;
        paused = true;
    }

    public static void onResume(){
        gameOn = true;
        paused = true;
        Thread t = new Thread(new MainGame());
        t.start();
    }

	@Override
	public void run() {
		KyleTime time = new KyleTime();
		time.startCount();
		while (gameOn)
		{
			time.waitFor(10);
			goodToRender=false;
			
			time.startCount();
			goodToRender=true;

		}
		
	}
	void sleep(long ms)
	{
		try {
			Thread.sleep(ms);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

}
