package gamecomponents;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import android.opengl.GLES20;

import com.kylelem.starkollider.riGraphicTools;

/**
 * Object of the orbital trapezoids.
 */
public class Trapezoid {

    /**
     * TODO:
     * Add more effects, and possibly varying speeds.
     */

    /**
     * Indicates the order in which the buffer points should be drawn when rendered.
     */
    private short indicies[]={0,1,3,2,3,0};
    /**
     * Point buffer. contains the points in groups of three that will be drawn to create triangles
     * by OpenGL.
     */
    private float buffer[]=new float[12];
    /**
     * Buffer for the point vertices.
     */
    private FloatBuffer vertexBuffer;
    /**
     * Buffer for the indices.
     */
    private ShortBuffer drawListBuffer;
    /**
     * Radial coordinates and dimensions for this ParticleChaser object.
     */
    private float angle, width, distance, length;
    /**
     * Values representing the center of the screen. Used for radial geometry and coordinates.
     */
    private float center_x=0,center_y=0;
    /**
     * When true, this indicates that upon next call of move() the trapezoid will be randomly reset.
     */
	boolean pendingReset = false;
    /**
     * When true, this indicates that the trapezoid has been tapped and now is being destroyed.
     */
    boolean consumption = false;
	private float seed;
    /**
     * This is the ParticleChaser that is tied to this trapezoid.
     */
    ParticleChaser particleChaser = new ParticleChaser();
    /**
     * This is the color of the center ball.
     */
	Color color = new Color();

    /**
     * Allows construction of a Trapezoid with specific coordinates.
     * @param angle Angle of the trapezoid. A value between 0 and 1.
     * @param width Width of trapezoid. Relative value.
     * @param distance Distance from center of the screen.
     * @param length The length of the trapezoid.
     * @param x The X coordinate of the center of rotation.
     * @param y The Y coordinate of the center of rotation.
     */
	public Trapezoid(float angle, float width, float distance, float length, float x, float y) {
		this.angle=angle;
		this.width=width;
		this.distance = distance;
		this.length=length;
		center_x=x;
		center_y=y;
		recalculate();
	}

    /**
     * Creates a Trapezoid object with random values.
     */
	public Trapezoid()
	{
		randomReset();
	}

    /**
     * Reassigns all of the location variables.
     * @param angle Angle of the trapezoid. A value between 0 and 1.
     * @param width Width of trapezoid. Relative value.
     * @param distance Distance from center of the screen.
     * @param length The length of the trapezoid.
     */
	public void reassign(float angle, float width, float distance, float length) {
		this.angle=angle;
		this.width=width;
		this.distance =distance;
		this.length=length;
		recalculate();
	}

    /**
     * Randomly assigns values to color and location variables.
     */
	public void randomReset()
	{
		pendingReset=false;
        consumption=false;
		angle = Random.random(0, 1);
		width = Random.random(.045f, .07f);
		distance = Random.random(1.4f, 1.5f);
		length = Random.random(0.15f, 0.4f);
		color.randomReset();
		center_x=0;
		center_y=0;
		particleChaser.disable();
		seed = Random.random(-0.5f,0.5f);
		recalculate();
	}

    /**
     * Moves the trapezoid toward or away from the center of roatation.
     * @param distance How far to move. Negative numbers move toward center, positive numbers away.
     * @return Whether or not the trapezoid is intersecting the center circle.
     */
	public boolean move(float distance, float hecticity)
	{

        float rotation = (hecticity*(seed*-1))/200f;
        this.angle+=rotation;

        if(angle>1)
        {
            angle=0;
        }
        if(angle<0)
        {
            angle=1;
        }

		distance += (hecticity*seed)/200f-hecticity/400f;
		if(pendingReset)
			randomReset();
        if(consumption)
        {
            length+=distance;
            particleChaser.move(distance,color,angle);
            if(length<=0)
                pendingReset=true;
        }
        else {
            this.distance += distance;
            if (this.distance < 0) {
                this.distance = 0;
                length += distance;
                if (this.distance < MainGame.centerCircle.radius)//intersection with circle
                {
                    if (length + this.distance < MainGame.centerCircle.radius)
                        pendingReset = true;
                    recalculate();
                    return true;
                }
            }
        }
		recalculate();
		return false;
	}

    /**
     * Rotates the trapezoid around the center of rotation.
     * @param rotation How far to rotate the trapezoid.
     */
	public void rotate(float rotation,float hecticity)
    {
		recalculate();
	}

    /**
     * Checks for intersection of click. If the click values are inside of the trapezoid's values,
     * the trapezoid is dimmed and consumtion flag is set to true.
     * @param radius Distance value of click.
     * @param deg Degree in radians of rotation.
     */
	public void click(float radius, float deg)
	{
		if(!consumption&&deg>(angle-width/2)&&deg<(float) (angle+width/2)&&radius< distance +length&&radius> distance)
		{
            color.dim();
            particleChaser.create(angle,width, distance);
            particleChaser.enable();
			consumption=true;
		}
	}

    /**
     * Recalculate the vertex points. Only call after trapezoid has been updated.
     */
	void recalculate()
	{
        //Get radian value from angle and width.
		float rad = (float) ((angle-width/2) * 2*Math.PI);

        //Set vertices in X, Y, Z order. Z is always 0
	    buffer[0] = (float) (center_x + distance * Math.cos(rad));
	    buffer[1] = (float) (center_y + distance * Math.sin(rad));
	    buffer[2]=0;
	    buffer[3] = (float) (center_x + (distance +length) * Math.cos(rad));
	    buffer[4] = (float) (center_y + (distance +length) * Math.sin(rad));
	    buffer[5]=0;
	    rad = (float) ((angle+width/2) * 2*Math.PI);
	    buffer[6] = (float) (center_x + distance * Math.cos(rad));
	    buffer[7] = (float) (center_y + distance * Math.sin(rad));
	    buffer[8]=0;
	    buffer[9] = (float) (center_x + (distance +length) * Math.cos(rad));
	    buffer[10] = (float) (center_y + (distance +length) * Math.sin(rad));
	    buffer[11]=0;

        //Create the vertexBuffer object from buffer[]
	    ByteBuffer bb = ByteBuffer.allocateDirect(buffer.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(buffer);
        vertexBuffer.position(0);

        //Create the drawListBuffer from indicies[]
        ByteBuffer dlb = ByteBuffer.allocateDirect(indicies.length * 2);
        dlb.order(ByteOrder.nativeOrder());
        drawListBuffer = dlb.asShortBuffer();
        drawListBuffer.put(indicies);
        drawListBuffer.position(0);
	}


    /**
     * Draws the Trapezoid. The color is set to that contained within this object.
     */
	public void draw(float[] m)
	{
		color.use();
        // get handle to vertex shader's vPosition member
        int mPositionHandle = GLES20.glGetAttribLocation(riGraphicTools.sp_SolidColor, "vPosition");
 
        // Enable generic vertex attribute array
        GLES20.glEnableVertexAttribArray(mPositionHandle);
 
        // Prepare the triangle coordinate data
        GLES20.glVertexAttribPointer(mPositionHandle, 3,
                                     GLES20.GL_FLOAT, false,
                                     0, vertexBuffer);
 
        // Get handle to shape's transformation matrix
        int mtrxhandle = GLES20.glGetUniformLocation(riGraphicTools.sp_SolidColor, "uMVPMatrix");
 
        // Apply the projection and view transformation
        GLES20.glUniformMatrix4fv(mtrxhandle, 1, false, m, 0);
 
        // Draw the triangle
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, drawListBuffer.capacity(),
                GLES20.GL_UNSIGNED_SHORT, drawListBuffer);
        drawListBuffer.clear();
        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPositionHandle);
        particleChaser.calculate();
        particleChaser.draw(m);
	}

}
