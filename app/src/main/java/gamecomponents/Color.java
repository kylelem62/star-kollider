package gamecomponents;

import android.R.color;
import android.opengl.GLES20;

import com.kylelem.starkollider.riGraphicTools;

/**
 * A custom Color class because Java library can't keep up with me.
 * @author Kyle Lemmon
 */
public class Color {

    /**
     * Color component values.
     */
    public float r=0,g=0,b=0,a=0;
    /**
     * Color constants
     */
    public static final Color RED = new Color(1,0,0,0.5f);
    public static final Color GREEN = new Color(0,1,0,0.5f);
    public static final Color BLUE = new Color(0,0,1,0.5f);
    public static final Color WHITE = new Color(1,1,1,0.5f);
    public static final Color BLACK = new Color(0,0,0,0.5f);

    /**
     * Construct a Color object with random values.
     */
	public Color() {
		r=Random.random(0, 1);
		g=Random.random(0, 1);
		b=Random.random(0, 1);
		a=1;
	}

    /**
     * Construct a Color object with defined values
     * @param r Red component.
     * @param g Green component.
     * @param b Blue component.
     * @param a Alpha component.
     */
	public Color(float r, float g, float b, float a){
		this.r=r;
		this.g=g;
		this.b=b;
		this.a=a;
	}

    /**
     * Adds the parameter color to this Color object. Averages 1/1000 weight.
     * TODO add option for different weight.
     * @param c Color to add to this one.
     */
	public void addColorTo(Color c)
	{
		r=(995*r+5*c.r)/1000;
		g=(995*g+5*c.g)/1000;
		b=(995*b+5*c.b)/1000;
	}

    /**
     * Set the Color to the active color in OpenGL
     */
	public void use(){
		riGraphicTools.setColor(r, g, b, a);
	}

    /**
     * Dim the color by a divisor of 2.
     */
    public void dim()
    {
        r/=2;
        g/=2;
        b/=2;
    }

    /**
     * Randomly reset the color to new values.
     */
	public void randomReset()
	{
        float bright = 1;
        float mid = Random.random(0, Random.random(0.0f,(2-MainGame.getObjective().getHecticity())/2f));


        float random = Random.random(0,3);
        if(random<1)
        {
            r=0;
            if(Random.random(-1,1)>0)
            {
                g=bright;
                b=mid;
            }
            else
            {
                b=bright;
                g=mid;
            }

        }
        else if (random<2)
        {
            g=0;
            if(Random.random(-1,1)>0)
            {
                r=bright;
                b=mid;
            }
            else
            {
                b=bright;
                r=mid;
            }
        }
        if(random>2)
        {
            b=0;
            if(Random.random(-1,1)>0)
            {
                g=bright;
                r=mid;
            }
            else
            {
                r=bright;
                g=mid;
            }
        }
		a=1;
		use();
	}

}
