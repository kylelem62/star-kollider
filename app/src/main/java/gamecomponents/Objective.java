package gamecomponents;

import android.graphics.Matrix;
import android.opengl.GLES20;
import android.util.Log;
import com.kylelem.starkollider.riGraphicTools;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * ColorDisplay shows the RGB values based on their relative intensity to one another.
 * Created by Kyle on 5/28/2014.
 * @author Kyle Lemmon
 */
public class Objective {

    public static final int OBJ_DOMINATE = 0,
                            OBJ_ELIMINATE = 1,
                            COL_RED = 0,
                            COL_GREEN = 1,
                            COL_BLUE = 2;

    float hecticity = 0.0f;
    float shecticity = 0.0f;
    int obj, objcolor;

    /**
     * Location variables.
     */
    float x=0,y=0,width=0,height=0;
    /**
     * Color variable that is dictates the display's content.
     */
    Color color;
    /**
     * Matrices of red, green, and blue values.
     */
    KyleMatrix  rMatrix = new KyleMatrix(4),
            gMatrix = new KyleMatrix(4),
            bMatrix = new KyleMatrix(4);

    /**
     * Constructs a new ColorDisplay with location values.
     * @param startX X value.
     * @param startY Y value.
     * @param sizeWidth Width.
     * @param sizeHeight Height.
     */
    public Objective(float startX, float startY, float sizeWidth, float sizeHeight)
    {
        x=startX;
        y=startY;
        width = sizeWidth;
        height = sizeHeight;
    }

    public void randomreset(){
        obj = (int)Random.random(0,1.99f);
        objcolor = (int)Random.random(0,2.99f);
        shecticity += 0.05f;
        if(shecticity>1)
            shecticity = 1;
        hecticity = shecticity;

    }

    public Color getColor(){
        if(obj == OBJ_DOMINATE){
            if(objcolor == COL_RED) {
                return Color.RED;
            }else
            if(objcolor == COL_BLUE) {
                return Color.BLUE;
            }else{
                return Color.GREEN;
            }
        }else{
            if(objcolor == COL_RED) {
                return Color.RED;
            }else
            if(objcolor == COL_BLUE) {
                return Color.BLUE;
            }else{
                return Color.GREEN;
            }
        }
    }

    public int getObj(){
        return obj;
    }

    /**
     * Set the Color to be used by the display.
     * @param c Color to set.
     * @see Color
     */
    public void setColor(Color c)
    {
        color = c;
    }

    public float getHecticity(){
        return hecticity;
    }

    /**
     * Recalculate all the vertices.
     */
    public void calculate()
    {
        if(obj == OBJ_DOMINATE){
            if(objcolor == COL_RED) {
                if (color.r >= 0.95f)
                    randomreset();
                hecticity = color.r+shecticity;
            }
            if(objcolor == COL_BLUE) {
                if (color.b >= 0.95f)
                    randomreset();
                hecticity = color.b+shecticity;
            }
            if(objcolor == COL_GREEN) {
                if (color.g >= 0.95f)
                    randomreset();
                hecticity = color.g+shecticity;
            }
        }
        if(obj == OBJ_ELIMINATE){
            if(objcolor == COL_RED) {
                if (color.r <= 0.05f)
                    randomreset();
                hecticity = 1-color.r+shecticity;
            }
            if(objcolor == COL_BLUE) {
                if (color.b <= 0.05f)
                    randomreset();
                hecticity = 1-color.b+shecticity;
            }
            if(objcolor == COL_GREEN) {
                if (color.g <= 0.05f)
                    randomreset();
                hecticity = 1-color.g+shecticity;
            }
        }
        float sum = color.r+color.b+color.g;
        float rSize = color.r/sum;
        float gSize = color.g/sum;
        float bSize = color.b/sum;

        float rEnd = rSize*width + x;
        float gEnd = gSize*width + rEnd;
        float bEnd = bSize*width + gEnd;


        rMatrix.setVertex(0, x, y, 0);
        rMatrix.setVertex(1, x, y + height, 0);
        rMatrix.setVertex(2,rEnd,y,0);
        rMatrix.setVertex(3,rEnd,y+height,0);
        rMatrix.indicies = new short[] {
                0,2,3,
                0,3,1
        };
        rMatrix.calculate();

        gMatrix.setVertex(0,rEnd,y,0);
        gMatrix.setVertex(1,rEnd,y+height,0);
        gMatrix.setVertex(2,gEnd,y,0);
        gMatrix.setVertex(3,gEnd,y+height,0);
        gMatrix.indicies = new short[] {
                0,2,3,
                0,3,1
        };
        gMatrix.calculate();

        bMatrix.setVertex(0,gEnd,y,0);
        bMatrix.setVertex(1,gEnd,y+height,0);
        bMatrix.setVertex(2,bEnd,y,0);
        bMatrix.setVertex(3,bEnd,y+height,0);
        bMatrix.indicies = new short[] {
                0,2,3,
                0,3,1
        };
        bMatrix.calculate();

    }

    private void drawdom(float[] m){
        if(objcolor == COL_RED){
            Color.WHITE.use();
            int mPositionHandle = GLES20.glGetAttribLocation(riGraphicTools.sp_SolidColor, "vPosition");
            GLES20.glEnableVertexAttribArray(mPositionHandle);
            GLES20.glVertexAttribPointer(mPositionHandle, 3,
                    GLES20.GL_FLOAT, false,
                    0, rMatrix.vertexBuffer);
            int mtrxhandle = GLES20.glGetUniformLocation(riGraphicTools.sp_SolidColor, "uMVPMatrix");
            GLES20.glUniformMatrix4fv(mtrxhandle, 1, false, m, 0);
            GLES20.glDrawElements(GLES20.GL_TRIANGLES, rMatrix.indicies.length,
                    GLES20.GL_UNSIGNED_SHORT, rMatrix.getShortBufferIndicies());
            GLES20.glDisableVertexAttribArray(mPositionHandle);
        }else
        if(objcolor == COL_BLUE){
            Color.WHITE.use();
            int mPositionHandle = GLES20.glGetAttribLocation(riGraphicTools.sp_SolidColor, "vPosition");
            GLES20.glEnableVertexAttribArray(mPositionHandle);
            GLES20.glVertexAttribPointer(mPositionHandle, 3,
                    GLES20.GL_FLOAT, false,
                    0, bMatrix.vertexBuffer);
            int mtrxhandle = GLES20.glGetUniformLocation(riGraphicTools.sp_SolidColor, "uMVPMatrix");
            GLES20.glUniformMatrix4fv(mtrxhandle, 1, false, m, 0);
            GLES20.glDrawElements(GLES20.GL_TRIANGLES, bMatrix.indicies.length,
                    GLES20.GL_UNSIGNED_SHORT, bMatrix.getShortBufferIndicies());
            GLES20.glDisableVertexAttribArray(mPositionHandle);
        }else
        if(objcolor == COL_GREEN){
            Color.WHITE.use();
            int mPositionHandle = GLES20.glGetAttribLocation(riGraphicTools.sp_SolidColor, "vPosition");
            GLES20.glEnableVertexAttribArray(mPositionHandle);
            GLES20.glVertexAttribPointer(mPositionHandle, 3,
                    GLES20.GL_FLOAT, false,
                    0, gMatrix.vertexBuffer);
            int mtrxhandle = GLES20.glGetUniformLocation(riGraphicTools.sp_SolidColor, "uMVPMatrix");
            GLES20.glUniformMatrix4fv(mtrxhandle, 1, false, m, 0);
            GLES20.glDrawElements(GLES20.GL_TRIANGLES, gMatrix.indicies.length,
                    GLES20.GL_UNSIGNED_SHORT, gMatrix.getShortBufferIndicies());
            GLES20.glDisableVertexAttribArray(mPositionHandle);
        }
    }

    private void draweli(float[] m){
        if(objcolor == COL_RED){
            Color.WHITE.use();
            int mPositionHandle = GLES20.glGetAttribLocation(riGraphicTools.sp_SolidColor, "vPosition");
            GLES20.glEnableVertexAttribArray(mPositionHandle);
            GLES20.glVertexAttribPointer(mPositionHandle, 3,
                    GLES20.GL_FLOAT, false,
                    0, gMatrix.vertexBuffer);
            int mtrxhandle = GLES20.glGetUniformLocation(riGraphicTools.sp_SolidColor, "uMVPMatrix");
            GLES20.glUniformMatrix4fv(mtrxhandle, 1, false, m, 0);
            GLES20.glDrawElements(GLES20.GL_TRIANGLES, gMatrix.indicies.length,
                    GLES20.GL_UNSIGNED_SHORT, gMatrix.getShortBufferIndicies());
            GLES20.glDisableVertexAttribArray(mPositionHandle);

            mPositionHandle = GLES20.glGetAttribLocation(riGraphicTools.sp_SolidColor, "vPosition");
            GLES20.glEnableVertexAttribArray(mPositionHandle);
            GLES20.glVertexAttribPointer(mPositionHandle, 3,
                    GLES20.GL_FLOAT, false,
                    0, bMatrix.vertexBuffer);
            mtrxhandle = GLES20.glGetUniformLocation(riGraphicTools.sp_SolidColor, "uMVPMatrix");
            GLES20.glUniformMatrix4fv(mtrxhandle, 1, false, m, 0);
            GLES20.glDrawElements(GLES20.GL_TRIANGLES, bMatrix.indicies.length,
                    GLES20.GL_UNSIGNED_SHORT, bMatrix.getShortBufferIndicies());
            GLES20.glDisableVertexAttribArray(mPositionHandle);
        }else
        if(objcolor == COL_BLUE){
            Color.WHITE.use();
            int mPositionHandle = GLES20.glGetAttribLocation(riGraphicTools.sp_SolidColor, "vPosition");
            GLES20.glEnableVertexAttribArray(mPositionHandle);
            GLES20.glVertexAttribPointer(mPositionHandle, 3,
                    GLES20.GL_FLOAT, false,
                    0, gMatrix.vertexBuffer);
            int mtrxhandle = GLES20.glGetUniformLocation(riGraphicTools.sp_SolidColor, "uMVPMatrix");
            GLES20.glUniformMatrix4fv(mtrxhandle, 1, false, m, 0);
            GLES20.glDrawElements(GLES20.GL_TRIANGLES, gMatrix.indicies.length,
                    GLES20.GL_UNSIGNED_SHORT, gMatrix.getShortBufferIndicies());
            GLES20.glDisableVertexAttribArray(mPositionHandle);

            mPositionHandle = GLES20.glGetAttribLocation(riGraphicTools.sp_SolidColor, "vPosition");
            GLES20.glEnableVertexAttribArray(mPositionHandle);
            GLES20.glVertexAttribPointer(mPositionHandle, 3,
                    GLES20.GL_FLOAT, false,
                    0, rMatrix.vertexBuffer);
            mtrxhandle = GLES20.glGetUniformLocation(riGraphicTools.sp_SolidColor, "uMVPMatrix");
            GLES20.glUniformMatrix4fv(mtrxhandle, 1, false, m, 0);
            GLES20.glDrawElements(GLES20.GL_TRIANGLES, rMatrix.indicies.length,
                    GLES20.GL_UNSIGNED_SHORT, rMatrix.getShortBufferIndicies());
            GLES20.glDisableVertexAttribArray(mPositionHandle);
        }else
        if(objcolor == COL_GREEN){
            Color.WHITE.use();
            int mPositionHandle = GLES20.glGetAttribLocation(riGraphicTools.sp_SolidColor, "vPosition");
            GLES20.glEnableVertexAttribArray(mPositionHandle);
            GLES20.glVertexAttribPointer(mPositionHandle, 3,
                    GLES20.GL_FLOAT, false,
                    0, rMatrix.vertexBuffer);
            int mtrxhandle = GLES20.glGetUniformLocation(riGraphicTools.sp_SolidColor, "uMVPMatrix");
            GLES20.glUniformMatrix4fv(mtrxhandle, 1, false, m, 0);
            GLES20.glDrawElements(GLES20.GL_TRIANGLES, rMatrix.indicies.length,
                    GLES20.GL_UNSIGNED_SHORT, rMatrix.getShortBufferIndicies());
            GLES20.glDisableVertexAttribArray(mPositionHandle);

            mPositionHandle = GLES20.glGetAttribLocation(riGraphicTools.sp_SolidColor, "vPosition");
            GLES20.glEnableVertexAttribArray(mPositionHandle);
            GLES20.glVertexAttribPointer(mPositionHandle, 3,
                    GLES20.GL_FLOAT, false,
                    0, bMatrix.vertexBuffer);
            mtrxhandle = GLES20.glGetUniformLocation(riGraphicTools.sp_SolidColor, "uMVPMatrix");
            GLES20.glUniformMatrix4fv(mtrxhandle, 1, false, m, 0);
            GLES20.glDrawElements(GLES20.GL_TRIANGLES, bMatrix.indicies.length,
                    GLES20.GL_UNSIGNED_SHORT, bMatrix.getShortBufferIndicies());
            GLES20.glDisableVertexAttribArray(mPositionHandle);
        }
    }

    /**
     * Draw the display.
     * @param m OpenGL Matrix
     */
    public void draw(float[] m){
        if(obj == OBJ_DOMINATE)
            drawdom(m);
        else if (obj==OBJ_ELIMINATE)
            draweli(m);

    }
}

