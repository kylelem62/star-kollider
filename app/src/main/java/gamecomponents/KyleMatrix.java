package gamecomponents;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
import java.nio.ShortBuffer;

/**
 * Created by Kyle on 5/28/2014.
 */
public class KyleMatrix {
    public short[] indicies;
    public float[][] verticies;
    float realVerticies[];
    FloatBuffer vertexBuffer;
    ShortBuffer indexBuffer;
    public KyleMatrix(int size)
    {
        indicies=new short[size];
        verticies=new float[size][3];
        realVerticies=new float[size*3];
        for (byte i = 0; i<size; i++)
            indicies[i]=i;
    }

    void setVertex(int vertex, float x, float y, float z)
    {
        verticies[vertex][0]=x;
        verticies[vertex][1]=y;
        verticies[vertex][2]=z;
    }

    public void calculate()
    {
        for(int i = 0; i<verticies.length*3; i++)
        {
            realVerticies[i] = verticies[i/3][i%3];
        }

        ByteBuffer bb = ByteBuffer.allocateDirect(realVerticies.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(realVerticies);
        vertexBuffer.position(0);

        ByteBuffer dlb = ByteBuffer.allocateDirect(indicies.length * 2);
        dlb.order(ByteOrder.nativeOrder());
        indexBuffer = dlb.asShortBuffer();
        indexBuffer.put(indicies);
        indexBuffer.position(0);
    }

    public FloatBuffer getFloatBufferVerticies(){return vertexBuffer;}

    public ShortBuffer getShortBufferIndicies(){return indexBuffer;}

    public float[] getVertecies()
    {
        return realVerticies;
    }
}
