package gamecomponents;


import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import android.opengl.GLES20;

import com.kylelem.starkollider.riGraphicTools;


public class Circle {
	int vertexCount;
	float radius;
	public float center_x;
	public float center_y;
	int indexLength;
	short indicies[];
	float buffer[]; // (x,y,z) for each vertex
	public FloatBuffer vertexBuffer;
	public ShortBuffer drawListBuffer;
	public ShortBuffer sdrawListBuffer;
	//s
	public Circle(int vertexCount, float radius, float x, float y) {
		this.vertexCount=vertexCount;
		this.radius=radius;
		center_x=x;
		center_y=y;
		
		indicies = new short[3*(vertexCount-1)+1];
		buffer = new float[vertexCount*3];
		//create a buffer for vertex data
		
		int idx = 0;
		
		indicies[0]=0;
		indicies[1]=1;
		indicies[2]=2;
		
		//center vertex for triangle fan
		buffer[idx++] = center_x;
		buffer[idx++] = center_y;
		buffer[idx++] = 0;//useless z
		
		
		//outer vertices of the circle
		short indexIndex = 1;
		int outerVertexCount = vertexCount-1;
		int i =2;
		while(i<3*(vertexCount-1)-2)
		{
		    indicies[i+1]=0;
		    indicies[i+2]=indexIndex++;
		    indicies[i+3]=indexIndex;
		    i+=3;
		}
		for (i = 0; i < outerVertexCount; ++i){
		    float percent = (i / (float) (outerVertexCount-1));
		    float rad = (float) (percent * 2*Math.PI);
		    
		    //vertex position
		    float outer_x = (float) (center_x + radius * Math.cos(rad));
		    float outer_y = (float) (center_y + radius * Math.sin(rad));
		    
		    
		    buffer[idx++] = outer_x;
		    buffer[idx++] = outer_y;
		    buffer[idx++] = 0;//useless z
		    
		    
		}
		ByteBuffer bb = ByteBuffer.allocateDirect(buffer.length * 4);
        bb.order(ByteOrder.nativeOrder());
        vertexBuffer = bb.asFloatBuffer();
        vertexBuffer.put(buffer);
        vertexBuffer.position(0);
 
        indexLength=indicies.length;
        
        // initialize byte buffer for the draw list
        ByteBuffer dlb = ByteBuffer.allocateDirect(indicies.length * 2);
        dlb.order(ByteOrder.nativeOrder());
        drawListBuffer = dlb.asShortBuffer();
        drawListBuffer.put(indicies);
        drawListBuffer.position(0);
	}

	public void drawSlash(float[] m){
        short[] indy = new short[3*(vertexCount-1)+1];
        int i=0;
        while(i<3*(vertexCount-1)-2){

            indy[i]=(short) (i/3);
            indy[i+1]=(short) (i/3+1);
            indy[i+2]=(short) (vertexCount-i/3);
            i+=12;
        }

        // initialize byte buffer for the draw list
        ByteBuffer dlb = ByteBuffer.allocateDirect(indy.length * 2);
        dlb.order(ByteOrder.nativeOrder());
        sdrawListBuffer = dlb.asShortBuffer();
        sdrawListBuffer.put(indy);
        sdrawListBuffer.position(0);

        int mPositionHandle = GLES20.glGetAttribLocation(riGraphicTools.sp_SolidColor, "vPosition");
        GLES20.glEnableVertexAttribArray(mPositionHandle);
        GLES20.glVertexAttribPointer(mPositionHandle, 3,
                GLES20.GL_FLOAT, false,
                0, vertexBuffer);
        int mtrxhandle = GLES20.glGetUniformLocation(riGraphicTools.sp_SolidColor, "uMVPMatrix");
        GLES20.glUniformMatrix4fv(mtrxhandle, 1, false, m, 0);
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, indy.length,
                GLES20.GL_UNSIGNED_SHORT, sdrawListBuffer);
        GLES20.glDisableVertexAttribArray(mPositionHandle);
    }

	public void draw(float[] m)
	{
        // get handle to vertex shader's vPosition member
        int mPositionHandle = GLES20.glGetAttribLocation(riGraphicTools.sp_SolidColor, "vPosition");
 
        // Enable generic vertex attribute array
        GLES20.glEnableVertexAttribArray(mPositionHandle);
 
        // Prepare the triangle coordinate data
        GLES20.glVertexAttribPointer(mPositionHandle, 3,
                                     GLES20.GL_FLOAT, false,
                                     0, vertexBuffer);
 
        // Get handle to shape's transformation matrix
        int mtrxhandle = GLES20.glGetUniformLocation(riGraphicTools.sp_SolidColor, "uMVPMatrix");
 
        // Apply the projection and view transformation
        GLES20.glUniformMatrix4fv(mtrxhandle, 1, false, m, 0);
 
        // Draw the triangle
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, indexLength,
                GLES20.GL_UNSIGNED_SHORT, drawListBuffer);
        
        // Disable vertex array
        GLES20.glDisableVertexAttribArray(mPositionHandle);
	}

}
