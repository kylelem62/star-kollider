package gamecomponents;

import android.graphics.Matrix;
import android.opengl.GLES20;
import android.util.Log;
import com.kylelem.starkollider.riGraphicTools;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;

/**
 * ColorDisplay shows the RGB values based on their relative intensity to one another.
 * Created by Kyle on 5/28/2014.
 * @author Kyle Lemmon
 */
public class ColorDisplay {
    /**
     * Location variables.
     */
    float x=0,y=0,width=0,height=0;
    /**
     * Color variable that is dictates the display's content.
     */
    Color color;
    /**
     * Matrices of red, green, and blue values.
     */
    KyleMatrix  rMatrix = new KyleMatrix(4),
                gMatrix = new KyleMatrix(4),
                bMatrix = new KyleMatrix(4);

    /**
     * Constructs a new ColorDisplay with location values.
     * @param startX X value.
     * @param startY Y value.
     * @param sizeWidth Width.
     * @param sizeHeight Height.
     */
    public ColorDisplay(float startX, float startY, float sizeWidth, float sizeHeight)
    {
        x=startX;
        y=startY;
        width = sizeWidth;
        height = sizeHeight;
    }

    /**
     * Set the Color to be used by the display.
     * @param c Color to set.
     * @see Color
     */
    public void setColor(Color c)
    {
        color = c;
    }

    /**
     * Recalculate all the vertices.
     */
    public void calculate()
    {

        float sum = color.r+color.b+color.g;
        float rSize = color.r/sum;
        float gSize = color.g/sum;
        float bSize = color.b/sum;

        float rEnd = rSize*width + x;
        float gEnd = gSize*width + rEnd;
        float bEnd = bSize*width + gEnd;


        rMatrix.setVertex(0, x, y, 0);
        rMatrix.setVertex(1, x, y + height, 0);
        rMatrix.setVertex(2,rEnd,y,0);
        rMatrix.setVertex(3,rEnd,y+height,0);
        rMatrix.indicies = new short[] {
                0,2,3,
                0,3,1
        };
        rMatrix.calculate();

        gMatrix.setVertex(0,rEnd,y,0);
        gMatrix.setVertex(1,rEnd,y+height,0);
        gMatrix.setVertex(2,gEnd,y,0);
        gMatrix.setVertex(3,gEnd,y+height,0);
        gMatrix.indicies = new short[] {
                0,2,3,
                0,3,1
        };
        gMatrix.calculate();

        bMatrix.setVertex(0,gEnd,y,0);
        bMatrix.setVertex(1,gEnd,y+height,0);
        bMatrix.setVertex(2,bEnd,y,0);
        bMatrix.setVertex(3,bEnd,y+height,0);
        bMatrix.indicies = new short[] {
                0,2,3,
                0,3,1
        };
        bMatrix.calculate();



    }

    /**
     * Draw the display.
     * @param m OpenGL Matrix
     */
    public void draw(float[] m){

        //Draw code for rMatrix
        Color.RED.use();
        int mPositionHandle = GLES20.glGetAttribLocation(riGraphicTools.sp_SolidColor, "vPosition");
        GLES20.glEnableVertexAttribArray(mPositionHandle);
        GLES20.glVertexAttribPointer(mPositionHandle, 3,
                GLES20.GL_FLOAT, false,
                0, rMatrix.vertexBuffer);
        int mtrxhandle = GLES20.glGetUniformLocation(riGraphicTools.sp_SolidColor, "uMVPMatrix");
        GLES20.glUniformMatrix4fv(mtrxhandle, 1, false, m, 0);
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, rMatrix.indicies.length,
                GLES20.GL_UNSIGNED_SHORT, rMatrix.getShortBufferIndicies());
        GLES20.glDisableVertexAttribArray(mPositionHandle);

        //Draw code for gMatrix
        Color.GREEN.use();
        GLES20.glEnableVertexAttribArray(mPositionHandle);
        GLES20.glVertexAttribPointer(mPositionHandle, 3,
                GLES20.GL_FLOAT, false,
                0, gMatrix.getFloatBufferVerticies());
        GLES20.glUniformMatrix4fv(mtrxhandle, 1, false, m, 0);
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, gMatrix.indicies.length,
                GLES20.GL_UNSIGNED_SHORT, gMatrix.getShortBufferIndicies());
        GLES20.glDisableVertexAttribArray(mPositionHandle);

        //Draw code for bMatrix
        Color.BLUE.use();
        GLES20.glEnableVertexAttribArray(mPositionHandle);
        GLES20.glVertexAttribPointer(mPositionHandle, 3,
                GLES20.GL_FLOAT, false,
                0, bMatrix.getFloatBufferVerticies());
        GLES20.glUniformMatrix4fv(mtrxhandle, 1, false, m, 0);
        GLES20.glDrawElements(GLES20.GL_TRIANGLES, bMatrix.indicies.length,
                GLES20.GL_UNSIGNED_SHORT, bMatrix.getShortBufferIndicies());
        GLES20.glDisableVertexAttribArray(mPositionHandle);
    }
}

