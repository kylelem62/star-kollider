package com.kylelem.starkollider;

import gamecomponents.MainGame;
import android.content.Context;
import android.opengl.GLSurfaceView;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class GameView extends GLSurfaceView {

	public GameView(Context context)
	{
		super(context);
		setEGLContextClientVersion(2);
		
	}
	
	@Override
	public boolean onTouchEvent(MotionEvent me)
	{

		float x = (me.getX()-GameViewRenderer.mScreenWidth/2f)/GameViewRenderer.mScreenHeight;
		float y = (me.getY()-GameViewRenderer.mScreenHeight/2f)/GameViewRenderer.mScreenHeight;
		float radius = (float) Math.sqrt((x*x)+(y*y));
		float deg = (float) ((float) (Math.acos((x)/(radius)))/(2*Math.PI));
		if(y>0)
			deg=.5f+(.5f-deg);
		MainGame.click(radius, deg);
		return true;
	}
	
	public GameView(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	@Override
    public void onPause() {
        super.onPause();
        OpenGL.gameViewRenderer.onPause();
        //System.exit(0);
    }
 
    @Override
    public void onResume() {

        super.onResume();
        OpenGL.gameViewRenderer.onResume();
        //System.exit(0);
    }

}
