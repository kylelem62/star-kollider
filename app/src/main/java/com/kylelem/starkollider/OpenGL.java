package com.kylelem.starkollider;

import gamecomponents.MainGame;
import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnSystemUiVisibilityChangeListener;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

public class OpenGL extends Activity {

	static boolean navBarHidden = false;
	static GameView gameView;
	static GameViewRenderer gameViewRenderer;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        getWindow().requestFeature(Window.FEATURE_ACTION_BAR);
        setContentView(R.layout.activity_open_gl);
        gameView = new GameView(this);
        gameViewRenderer = new GameViewRenderer(this);
        gameView.setRenderer(gameViewRenderer);
        gameView.setRenderMode(GameView.RENDERMODE_CONTINUOUSLY);
        RelativeLayout layout = (RelativeLayout) findViewById(R.id.gamelayout);
        
        // Attach our surfaceview to our relative layout from our main layout.
        RelativeLayout.LayoutParams glParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT, RelativeLayout.LayoutParams.MATCH_PARENT);
        layout.addView(gameView, glParams);
    }
    
    @Override
    protected void onResume()
    {
    	super.onResume();
        activitySetup();
    }

    @Override
    protected void onPause(){
        super.onPause();

    }
    
    //ANDROID ACTIVITY CRAP GOES DOWN HERE
    @SuppressLint("InlinedApi") 
    void activitySetup()
    {
    	getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        if (Build.VERSION.SDK_INT > 15){
        	final View decorView = getWindow().getDecorView();
        	// Hide the status bar.
        	int uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        			| View.SYSTEM_UI_FLAG_LOW_PROFILE;
        	if(Build.VERSION.SDK_INT > 18) {//kitkat
        		uiOptions = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
        	            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
        	            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
        	            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
        	            | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
        	            | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY;
        	}
        	// Remember that you should never show the action bar if the
        	// status bar is hidden, so hide that too if necessary.
        	decorView.setSystemUiVisibility(uiOptions);
        	final int uiFinal = uiOptions;
        	decorView.setOnSystemUiVisibilityChangeListener(new OnSystemUiVisibilityChangeListener(){
				@Override
				public void onSystemUiVisibilityChange(int visibilityFlag) {
					decorView.setSystemUiVisibility(uiFinal);
				}
    		});
        }
    }
}
