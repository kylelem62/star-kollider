package com.kylelem.starkollider;

import android.opengl.GLES20;
import android.util.Log;

public class riGraphicTools {
 
    // Program variables
    public static int sp_SolidColor;
 
    /* SHADER Solid
     *
     * This shader is for rendering a colored primitive.
     *
     */
    public static float r=0,g=0,b=0,a=0;
    static int objColorLoc=0;
    public static final String vs_SolidColor =
        "uniform    mat4        uMVPMatrix;" +
        "attribute  vec4        vPosition;" +
        "void main() {" +
        "  gl_Position = uMVPMatrix * vPosition;" +
        "}";
 
    public static String fs_SolidColor =
        "precision mediump float;" +
        "uniform vec4 objColor;" +
        "void main() {" +
        "  gl_FragColor = objColor;" +
        "}";
   
    
    public static void init()
    {
    	
    	// Create the shaders
        int vertexShader = riGraphicTools.loadShader(GLES20.GL_VERTEX_SHADER, riGraphicTools.vs_SolidColor);
        int fragmentShader = riGraphicTools.loadShader(GLES20.GL_FRAGMENT_SHADER, riGraphicTools.fs_SolidColor);
 
        riGraphicTools.sp_SolidColor = GLES20.glCreateProgram();             // create empty OpenGL ES Program
        GLES20.glAttachShader(riGraphicTools.sp_SolidColor, vertexShader);   // add the vertex shader to program
        GLES20.glAttachShader(riGraphicTools.sp_SolidColor, fragmentShader); // add the fragment shader to program
        GLES20.glLinkProgram(riGraphicTools.sp_SolidColor);                  // creates OpenGL ES program executables
        GLES20.glUseProgram(sp_SolidColor);
        objColorLoc=GLES20.glGetUniformLocation(sp_SolidColor, "objColor");
    }
    
    public static int loadShader(int type, String shaderCode){
    	
        // create a vertex shader type (GLES20.GL_VERTEX_SHADER)
        // or a fragment shader type (GLES20.GL_FRAGMENT_SHADER)
        int shader = GLES20.glCreateShader(type);
 
        // add the source code to the shader and compile it
        GLES20.glShaderSource(shader, shaderCode);
        GLES20.glCompileShader(shader);
        // return the shader
        return shader;
    }
    
    public static void setColor(float r, float g, float b, float a)
    {
    	GLES20.glUniform4f(objColorLoc, r, g, b, a);
    }
}