package com.kylelem.starkollider;

import gamecomponents.MainGame;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.nio.ShortBuffer;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

import android.content.Context;
import android.opengl.GLES20;
import android.opengl.GLSurfaceView;
import android.opengl.GLU;
import android.opengl.Matrix;
import android.util.AttributeSet;
import android.util.Log;

public class GameViewRenderer implements GLSurfaceView.Renderer {
	 // Our matrices
    private final float[] mtrxProjection = new float[16];
    private final float[] mtrxView = new float[16];
    private final float[] mtrxProjectionAndView = new float[16];

    // Geometric variables
    public static float vertices[];
    public static short indices[];
    public FloatBuffer vertexBuffer;
    public ShortBuffer drawListBuffer;
 
    // Our screenresolution
    public static float   mScreenWidth = 1280;
    public static float   mScreenHeight = 768;
 
    // Misc
    Context mContext;
    long mLastTime;
    int mProgram;
 
    public GameViewRenderer(Context c)
    {
        mContext = c;
        mLastTime = System.currentTimeMillis() + 100;
    }
 
    public void onPause()
    {
        /* Do stuff to pause the renderer */
        MainGame.onPause();
    }
 
    public void onResume()
    {
        /* Do stuff to resume the renderer */
        mLastTime = System.currentTimeMillis();
        MainGame.onResume();
    }
 
    @Override
    public void onDrawFrame(GL10 unused) {
 
        // Get the current time
        long now = System.currentTimeMillis();
 
        // We should make sure we are valid and sane
        if (mLastTime > now) return;
 
        // Get the amount of time the last frame took.
        long elapsed = now - mLastTime;
 
        // Update our example
 
        // Render our example
        if (!MainGame.paused)
            Render(mtrxProjectionAndView);
 
        // Save the current time to see how long it took <img src="http://androidblog.reindustries.com/wp-includes/images/smilies/icon_smile.gif" alt=":)" class="wp-smiley"> .
        mLastTime = now;
 
    }
 
    private void Render(float[] m) {
    	
        // clear Screen and Depth Buffer, we have set the clear color as black.
        GLES20.glClear(GLES20.GL_COLOR_BUFFER_BIT | GLES20.GL_DEPTH_BUFFER_BIT);

        MainGame.draw(m);
    }
  boolean created = false;
    @Override
    public void onSurfaceChanged(GL10 gl, int width, int height) {
 
        // We need to know the current width and height.
        mScreenWidth = width;
        mScreenHeight = height;


        // Redo the Viewport, making it fullscreen.
        GLES20.glViewport(0, 0, (int)mScreenWidth, (int)mScreenHeight);



        // Clear our matrices
        for(int i=0;i<16;i++)
        {
            mtrxProjection[i] = 0.0f;
            mtrxView[i] = 0.0f;
            mtrxProjectionAndView[i] = 0.0f;
        }
        float scaleWidth = (float) width/height;
        // Setup our screen width and height for normal sprite translation.
        Matrix.orthoM(mtrxProjection, 0, -scaleWidth/2f, scaleWidth/2f, -0.5f, 0.5f, 0, 50);
        //Matrix.orthoM(mtrxProjection, 0, 0, mScreenWidth, 0, mScreenHeight, 0, 50);
        // Set the camera position (View matrix)
        Matrix.setLookAtM(mtrxView, 0, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 1.0f, 0.0f);
        // Calculate the projection and view transformation
        Matrix.multiplyMM(mtrxProjectionAndView, 0, mtrxProjection, 0, mtrxView, 0);
        riGraphicTools.init();
        GLES20.glEnableVertexAttribArray(0);
      
 
    }
 
    @Override
    public void onSurfaceCreated(GL10 gl, EGLConfig config) {
        if(!created)
        {
            MainGame.glInit();
            created=true;
        }

        // Set the clear color to black
        GLES20.glClearColor(0.0f, 0.0f, 0.0f, 1);
 
       
    }
 


	

}
